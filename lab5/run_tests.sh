#!/bin/bash

for nrows in 4 8 16
do
	for ncols in 4 8 16
	do
		echo "* $nrows/$ncols"

		mem_circuit="${nrows}x${ncols}_6T_SRAM_mem.spi"
		precharge_mesure="precharge_measure_${nrows}x${ncols}_6T_SRAM_mem.cir"
		access_measure="access_measure_${nrows}x${ncols}_6T_SRAM_mem.cir"
		write_measure="write_measure_${nrows}x${ncols}_6T_SRAM_mem.cir"

		./memgen.py $nrows $ncols > "$mem_circuit"

		./precharge_mesure_gen.py $nrows $ncols > "$precharge_mesure"

		./access_measure_gen.py $nrows $ncols > "$access_measure"

		./write_measure_gen.py $nrows $ncols > "$write_measure"

		ngspice -a "$precharge_mesure" 2>/dev/null | grep "tprecharge"
		ngspice -a "$access_measure" 2>/dev/null | grep "taccess"
		ngspice -a "$write_measure" 2>/dev/null | grep "twrite"

		rm -f "$mem_circuit"
		rm -f "$precharge_mesure"
		rm -f "$access_measure"
		rm -f "$write_measure"
	done
done
