#!/usr/bin/env python3

import sys
import math

# Pre-measured
precharge_times = {
	(4, 4): 2.938294e-10,
	(4, 8): 2.951408e-10,
	(4, 16): 2.979901e-10,
	(8, 4): 4.294069e-10,
	(8, 8): 4.339872e-10,
	(8, 16): 4.310943e-10,
	(16, 4): 6.981290e-10,
	(16, 8): 6.975971e-10,
	(16, 16): 7.103168e-10
}

def usage():
	print("access_mesure_gen.py by Sergi Granell")
	print("Usage:\n\t", sys.argv[0], "nrows ncols")

def main():
	if (len(sys.argv) < 3):
		usage()
		return

	nrows = int(sys.argv[1])
	ncols = int(sys.argv[2])

	if not (nrows, ncols) in precharge_times:
		print("Precharge time for {}x{} missing".format(nrows, ncols))
		return

	log2_nrows = int(math.log(nrows, 2))
	wordline_dec_size = log2_nrows + 1

	print("* nrows:", nrows)
	print("* ncols:", ncols)
	print("")

	print(".INCLUDE model.spi")
	print(".INCLUDE {}x{}_6T_SRAM_mem.spi".format(nrows, ncols))
	print("")

	print(".PARAM PRECHARGE_TIME = {}".format(precharge_times[(nrows, ncols)]))
	print("")

	print("x1 precharge_b worldline_en write_en {} {} {} 1 0 {}x{}_6T_SRAM_mem".format(
		# Address bits
		" ".join(["A" + str(a) for a in range(log2_nrows)]),
		# Output (read)
		" ".join(["O{}".format(c) for c in range(ncols)]),
		# Input (write)
		" ".join(["I{}".format(c) for c in range(ncols)]),
		nrows,
		ncols
		)
	)
	print("")

	print("VCC 1 0 DC 5V")
	print("")
	print("Vprecharge precharge_b 0 pulse(0 5 PRECHARGE_TIME)")
	print("")
	print("Vwl_en worldline_en 0 pulse(0 5 PRECHARGE_TIME)")
	print("")
	print("Vwe write_en 0 DC 0V")
	print("")
	print("\n".join(["Va{} A{} 0 DC 0V".format(a, a) for a in range(log2_nrows)]))
	print("")
	print("\n".join(["Vin{} I{} 0 DC 0V".format(c, c) for c in range(ncols)]))
	print("")
	print("\n".join([".IC V(x1.BL{}) = 0V".format(c) for c in range(ncols)]))
	print("\n".join([".IC V(x1.BL{}_b) = 0V".format(c) for c in range(ncols)]))
	print("")
	print("\n".join([".IC V(O{}) = 0V".format(c) for c in range(ncols)]))
	print("")
	print(".control")
	print("")
	print("TRAN 75ps 10ns")
	print("")
	print("MEAS TRAN taccess TRIG AT=0 TARG V(O0) VAL=4 RISE=1")
	print("")
	print("quit")
	print("")
	print(".endc")
	print("")
	print(".END")
	print("")

if __name__ == "__main__":
	main()
