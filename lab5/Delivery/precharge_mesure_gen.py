#!/usr/bin/env python3

import sys
import math

def usage():
	print("precharge_mesure_gen.py by Sergi Granell")
	print("Usage:\n\t", sys.argv[0], "nrows ncols")

def main():
	if (len(sys.argv) < 3):
		usage()
		return

	nrows = int(sys.argv[1])
	ncols = int(sys.argv[2])

	log2_nrows = int(math.log(nrows, 2))
	wordline_dec_size = log2_nrows + 1

	print("* nrows:", nrows)
	print("* ncols:", ncols)
	print("")

	print(".INCLUDE model.spi")
	print(".INCLUDE {}x{}_6T_SRAM_mem.spi".format(nrows, ncols))
	print("")

	print("x1 precharge_b worldline_en write_en {} {} {} 1 0 {}x{}_6T_SRAM_mem".format(
		# Address bits
		" ".join(["A" + str(a) for a in range(log2_nrows)]),
		# Output (read)
		" ".join(["O{}".format(c) for c in range(ncols)]),
		# Input (write)
		" ".join(["I{}".format(c) for c in range(ncols)]),
		nrows,
		ncols
		)
	)
	print("")

	print("VCC 1 0 DC 5V")
	print("")
	print("Vprecharge precharge_b 0 pulse(5 0 1000ps)")
	print("")
	print("Vwl_en worldline_en 0 DC 0V")
	print("")
	print("Vwe write_en 0 DC 0V")
	print("")
	print("\n".join(["Va{} A{} 0 DC 0V".format(a, a) for a in range(log2_nrows)]))
	print("")
	print("\n".join(["Vin{} I{} 0 DC 0V".format(c, c) for c in range(ncols)]))
	print("")
	print("\n".join([".IC V(x1.BL{}) = 0V".format(c) for c in range(ncols)]))
	print("\n".join([".IC V(x1.BL{}_b) = 0V".format(c) for c in range(ncols)]))
	print("")
	print(".control")
	print("")
	print("TRAN 75ps 10ns")
	print("")
	print("MEAS TRAN tprecharge TRIG V(precharge_b) VAL=4 FALL=1 TARG V(x1.BL0) VAL=4 RISE=1")
	print("")
	print("quit")
	print("")
	print(".endc")
	print("")
	print(".END")
	print("")

if __name__ == "__main__":
	main()
