#!/usr/bin/env python3

import sys
import math

def usage():
	print("memgen.py by Sergi Granell")
	print("Usage:\n\t", sys.argv[0], "nrows ncols")

def main():
	if (len(sys.argv) < 3):
		usage()
		return

	nrows = int(sys.argv[1])
	ncols = int(sys.argv[2])

	log2_nrows = int(math.log(nrows, 2))
	wordline_dec_size = log2_nrows + 1

	print("* nrows:", nrows)
	print("* ncols:", ncols)
	print("")

	print(".INCLUDE 6T_SRAM_cell.spi")
	print(".INCLUDE not.spi")
	print(".INCLUDE precharge_circuit.spi")
	print(".INCLUDE write_driver.spi")
	print(".INCLUDE diff_pair_amp.spi")
	print("")

	print(".SUBCKT {}x{}_6T_SRAM_mem precharge_b worldline_en write_en {} {} {} 1 0".format(
		nrows,
		ncols,
		# Address bits
		" ".join(["A" + str(a) for a in range(log2_nrows)]),
		# Output (read)
		" ".join(["O{}".format(c) for c in range(ncols)]),
		# Input (write)
		" ".join(["I{}".format(c) for c in range(ncols)])
		)
	)
	print("")

	# Wordline decoder (AND gate) subcircuit definition
	print(".SUBCKT and{} {} Q 1 0".format(
		wordline_dec_size,
		" ".join(["X" + str(r) for r in range(wordline_dec_size)])
		)
	)
	for i in range(0, wordline_dec_size):
		print("\tM_X{}_p 1 X{} Q_b 1 tp L=1U W=2U AS=6P AD=6P PS=10U PD=10U".format(i, i))

		# TODO: Why NMOS L=1.1U to work?
		if i == 0:
			print("\tM_X0_n Q_b X0 N1 0 tn L=1.1U W=1U AS=3P AD=3P PS=6U PD=6U")
		elif i == wordline_dec_size - 1:
			print("\tM_X{}_n N{} X{} 0 0 tn L=1.1U W=1U AS=3P AD=3P PS=6U PD=6U".format(i, i, i))
		else:
			print("\tM_X{}_n N{} X{} N{} 0 tn L=1.1U W=1U AS=3P AD=3P PS=6U PD=6U".format(i, i, i, i + 1))

		print("")
	print("\tX_Q_not Q_b Q 1 0 not")
	print(".ENDS and{}".format(wordline_dec_size))
	print("")

	# Generate inverse address bits
	for i in range(0, log2_nrows):
		print("X_A{}_not A{} A{}_b 1 0 not".format(i, i, i))
	print("")

	# Worldline decoders
	for r in range(0, nrows):
		print("X_decoder_WL{} worldline_en {} WL{} 1 0 and{}".format(
			r,
			" ".join(["A" + str(i) + ("" if r & (1 << i) else "_b" ) for i in range(log2_nrows)]),
			r,
			wordline_dec_size
			)
		)
	print("")

	# Memory cells
	for r in range(0, nrows):
		print("* Row {}".format(r))
		for c in range(0, ncols):
			print("X_cell_{}_{} WL{} BL{} BL{}_b 1 0 6T_SRAM_cell".format(r, c, r, c, c))
		print("")

	# Bitline precharge conditioning
	for c in range(0, ncols):
		print("X_precharge_BL{} BL{} BL{}_b precharge_b 1 0 precharge_circuit".format(c, c, c))
	print("")

	# Write drivers
	for c in range(0, ncols):
		print("X_write_driver_BL{} BL{} BL{}_b I{} write_en 1 0 write_driver".format(c, c, c, c))
	print("")

	# Sense amplifiers
	for c in range(0, ncols):
		print("X_sense_amp_BL{} BL{} BL{}_b O{} O{}_b 1 0 diff_pair_amp".format(c, c, c, c, c))

	print("")

	# Initial cell values
	for r in range(0, nrows):
		for c in range(0, ncols):
			print(".IC V(X_cell_{}_{}.Q) = 5".format(r, c))
			print(".IC V(X_cell_{}_{}.Q_b) = 0".format(r, c))
		print("")

	print(".ENDS {}x{}_6T_SRAM_mem".format(nrows, ncols))


if __name__ == "__main__":
	main()
